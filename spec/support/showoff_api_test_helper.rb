module ShowoffAPITestHelper
  def failure_reponse
    Failure(ApiStruct::Errors::Client.new(body: { code: '0', message: 'server error', data: nil }))
  end

  def api_credentials
    { client_id: ShowoffAPI::CLIENT_ID, client_secret: ShowoffAPI::CLIENT_SECRET }
  end

  def auth_header(token)
    { 'Authorization' => "Bearer #{token}" }
  end
end
