require 'rails_helper'

describe ShowoffAPI::Clients::Widgets do
  let(:client) { described_class.new }
  let(:widget) { build(:widget) }
  let(:token) { 'access_token' }
  let(:auth_header) { { 'Authorization' => "Bearer #{token}" } }

  describe '#visible' do
    let(:url) { "#{client.api_root}/#{client.default_path}/visible" }
    let(:response_body) do
      {
        code: 0,
        message: 'Success',
        data: {
          widgets: attributes_for_list(:widget, 2)
        }
      }
    end

    context 'With valid params' do
      before do
        stub_request(:get, url).with(query: api_credentials).to_return(status: 200, body: response_body.to_json)
        @response = client.visible
      end

      it 'responds with success' do
        expect(@response.success?).to be_truthy
      end

      it 'responds with valid response body' do
        expect(@response.value![:widgets]).to eq response_body[:data][:widgets]
      end
    end

    context 'With serach term' do
      let(:params) { { term: 'none' }.merge(api_credentials) }
      let(:response_body) { { code: 0, message: 'Success', data: { widgets: [] } } }

      before do
        stub_request(:get, url).with(query: params).to_return(status: 200, body: response_body.to_json)
        @response = client.visible('none')
      end

      it 'responds with success' do
        expect(@response.success?).to be_truthy
      end

      it 'responds with empty widgets' do
        expect(@response.value![:widgets]).to eq response_body[:data][:widgets]
      end
    end
  end

  describe '#create' do
    let(:url) { "#{client.api_root}/#{client.default_path}" }
    let(:auth_header) { { 'Authorization' => "Bearer #{token}" } }

    context 'With valid params' do
      let(:widget) { attributes_for(:widget) }
      let(:request_body) { { widget: widget } }
      let(:response_body) { { code: 0, message: 'Success', data: { widget: widget } } }

      before do
        stub_request(:post, url).with(body: request_body, headers: auth_header).to_return(status: 200, body: response_body.to_json)
        @response = client.create(token: token, params: request_body)
      end

      it 'responds with success' do
        expect(@response.success?).to be_truthy
      end

      it 'responds with valid response body' do
        expect(@response.value!).to eq request_body
      end
    end

    context 'With Invalid params' do
      let(:widget) { attributes_for(:widget, name: '') }
      let(:request_body) { { widget: widget } }
      let(:response_body) { { code: 0, message: 'Errro', data: nil } }

      before do
        stub_request(:post, url).with(body: request_body, headers: auth_header).to_return(status: 422, body: response_body.to_json)
        @response = client.create(token: token, params: request_body)
      end

      it 'responds with failure' do
        expect(@response.success?).to be_falsy
      end
    end
  end

  describe '#update' do
    let(:url) { "#{client.api_root}/#{client.default_path}/#{widget.id}" }

    context 'With valid params' do
      let(:request_body) { { widget: widget.as_json } }
      let(:response_body) { { code: 0, message: 'Success', data: { widget: widget } } }

      before do
        stub_request(:put, url).with(body: request_body, headers: auth_header).to_return(status: 200, body: response_body.to_json)
        @response = client.update(token: token, id: widget.id, params: request_body)
      end

      it 'responds with success' do
        expect(@response.success?).to be_truthy
      end
    end
  end

  describe '#delete' do
    let(:url) { "#{client.api_root}/#{client.default_path}/#{widget.id}" }

    context 'With valid params' do
      let(:request_body) { { widget: widget.as_json } }
      let(:response_body) { { code: 0, message: 'Success', data: { widget: nil } } }

      before do
        stub_request(:delete, url).with(headers: auth_header).to_return(status: 200, body: response_body.to_json)
        @response = client.destroy(token: token, id: widget.id)
      end

      it 'responds with success' do
        expect(@response.success?).to be_truthy
      end
    end
  end
end
