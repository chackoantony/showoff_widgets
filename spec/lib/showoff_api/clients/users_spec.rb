require 'rails_helper'

describe ShowoffAPI::Clients::Users do
  let(:client) { described_class.new }
  let(:url) { "#{client.api_root}/#{client.default_path}" }
  let(:auth_header) { { 'Authorization' => "Bearer #{token}" } }
  let(:token) { 'test_token' }

  describe '#create' do
    let(:request_body) { { user: user_params }.merge(api_credentials) }
    let(:response_body) do
      {
        code: 0,
        message: 'Success',
        data: {
          user: user_params,
          token: {
            access_token: 'test_access_token',
            refresh_token: 'test_refresh_token'
          }
        }
      }
    end

    context 'With valid params' do
      let(:user_params) { attributes_for(:user) }
      before do
        stub_request(:post, url).with(body: request_body).to_return(status: 200, body: response_body.to_json)
        @response = client.create(request_body)
      end

      it 'responds with success' do
        expect(@response.success?).to be_truthy
      end

      it 'responds with valid response body' do
        expect(@response.value![:user]).to eq user_params
      end

      it 'responds with access_token' do
        expect(@response.value!.dig(:token, :access_token)).to eq response_body.dig(:data, :token, :access_token)
      end
    end

    context 'With invalid params' do
      let(:user_params) { attributes_for(:user, email: '') }
      let(:response_body) { { code: 3, message: 'Email cant be blank', data: nil } }

      before do
        stub_request(:post, url).with(body: request_body).to_return(status: 422, body: response_body.to_json)
        @response = client.create(request_body)
      end

      it 'responds with failure' do
        expect(@response.success?).to be_falsy
      end

      it 'responds with error message' do
        expect(@response.failure.body[:message]).to eq response_body[:message]
      end
    end
  end

  describe '#sign_in' do
    let(:url) { "#{client.api_root}/oauth/token" }
    let(:request_body) { login_params.merge(api_credentials) }
    let(:response_body) do
      {
        code: 0,
        message: 'Success',
        data: {
          token: {
            access_token: 'test_access_token',
            refresh_token: 'test_refresh_token'
          }
        }
      }
    end

    context 'With valid params' do
      let(:login_params) { attributes_for(:user).slice(:email, :password).merge(grant_type: 'password') }

      before do
        stub_request(:post, url).with(body: request_body).to_return(status: 200, body: response_body.to_json)
        @response = client.sign_in(request_body)
      end

      it 'responds with success' do
        expect(@response.success?).to be_truthy
      end

      it 'responds with access_token' do
        expect(@response.value![:token][:access_token]).to eq response_body[:data][:token][:access_token]
      end
    end

    context 'With invalid params' do
      let(:login_params) { attributes_for(:user, email: '').slice(:email, :password).merge(grant_type: 'password') }
      let(:response_body) { { code: 3, message: 'There was an error logging in. Please try again', data: nil } }

      before do
        stub_request(:post, url).with(body: request_body).to_return(status: 422, body: response_body.to_json)
        @response = client.sign_in(request_body)
      end

      it 'responds with failure' do
        expect(@response.success?).to be_falsy
      end

      it 'responds with error message' do
        expect(@response.failure.body[:message]).to eq response_body[:message]
      end
    end
  end

  describe '#show' do
    let(:url) { "#{client.api_root}/#{client.default_path}/#{user.id}" }

    context 'With valid id' do
      let(:user) { build(:user) }
      let(:response_body) { { code: 0, message: 'Success', data: { user: user } } }

      before do
        stub_request(:get, url).with(headers: auth_header).to_return(status: 200, body: response_body.to_json)
        @response = client.show(id: user.id, token: token)
      end

      it 'responds with success' do
        expect(@response.success?).to be_truthy
      end

      it 'responds with valid response body' do
        expect(@response.value![:user][:email]).to eq user.email
      end
    end

    context 'With Invalid id' do
      let(:user) { build(:user, id: 'none') }
      let(:response_body) { { code: 0, message: 'Success', data: nil } }

      before do
        stub_request(:get, url).with(headers: auth_header).to_return(status: 422, body: response_body.to_json)
        @response = client.show(id: user.id, token: token)
      end

      it 'responds with failure' do
        expect(@response.success?).to be_falsy
      end
    end
  end

  describe '#user_widgets' do
    let(:user) { build(:user) }
    let(:widgets) { build_list(:widget, 2) }

    context 'With valid user id' do
      let(:url) { "#{client.api_root}/#{client.default_path}/#{user.id}/widgets" }
      let(:response_body) { { code: 0, message: 'Success', data: { widgets: widgets } } }

      before do
        stub_request(:get, url).with(query: api_credentials, headers: auth_header)
                               .to_return(status: 200, body: response_body.to_json)
        @response = client.user_widgets(id: user.id, token: token)
      end

      it 'responds with success' do
        expect(@response.success?).to be_truthy
      end

      it 'responds with valid response body' do
        expect(@response.value![:widgets].length).to eq 2
      end
    end
  end

  describe '#me' do
    let(:url) { "#{client.api_root}/#{client.default_path}/me" }
    let(:user) { build(:user) }

    context 'With valid token' do
      let(:response_body) { { code: 0, message: 'Success', data: { user: user } } }

      before do
        stub_request(:get, url).with(headers: auth_header).to_return(status: 200, body: response_body.to_json)
        @response = client.me(token)
      end

      it 'responds with success' do
        expect(@response.success?).to be_truthy
      end

      it 'responds with valid response body' do
        expect(@response.value![:user][:email]).to eq user.email
      end
    end
  end
end
