require 'rails_helper'

describe User do
  let(:user) { build(:user) }
  let(:api_client) { ShowoffAPI::Clients::Users }
  let(:client_service) { instance_double(api_client) }
  let(:token) { 'test_token' }
  before { allow(api_client).to receive(:new).and_return(client_service) }

  describe 'Validations' do
    it 'is valid with all valid attributes' do
      expect(user).to be_valid
    end

    it 'is not valid without first_name' do
      user.first_name = nil
      expect(user).to_not be_valid
    end

    it 'is not valid without email' do
      user.email = nil
      expect(user).to_not be_valid
    end

    it 'is not valid without valid email' do
      user.email = 'not_an_email'
      expect(user).to_not be_valid
    end

    it 'is not valid with invalid password_confirmation' do
      user.password_confirmation = 'not_same_password'
      expect(user).to_not be_valid
    end
  end

  describe '#save' do
    context 'with invalid user params' do
      before { user.email = '' }

      it 'returns false' do
        expect(user.save).to be_falsy
      end

      it 'sets error message' do
        user.save
        expect(user.errors[:email]).to be_present
      end
    end

    context 'with valid user params' do
      context 'with api server error' do
        before { allow(client_service).to receive(:create).and_return(failure_reponse) }

        it 'returns false' do
          expect(user.save).to be_falsy
        end

        it 'sets error message' do
          user.save
          expect(user.errors[:base]).to be_present
        end
      end

      context 'with api success request' do
        let(:response_body) do
          {
            user: user,
            token: {
              access_token: 'test_access_token',
              refresh_token: 'test_refresh_token'
            }
          }
        end

        before {  allow(client_service).to receive(:create).and_return(Success(response_body)) }

        it 'returns true' do
          expect(user.save).to be_truthy
        end

        it 'sets access_token' do
          user.save
          expect(user.access_token).to be_present
        end
      end
    end
  end

  describe '#sign_in' do
    context 'with invalid sign_in params' do
      before { allow(client_service).to receive(:sign_in).and_return(failure_reponse) }

      it 'returns false' do
        expect(user.sign_in).to be_falsy
      end

      it 'sets error message' do
        user.sign_in
        expect(user.errors[:base]).to be_present
      end
    end

    context 'with valid sign_in params' do
      let(:response_body) do
        {
          token: {
            access_token: 'test_access_token',
            refresh_token: 'test_refresh_token'
          }
        }
      end

      before { allow(client_service).to receive(:sign_in).and_return(Success(response_body)) }

      it 'returns true' do
        expect(user.sign_in).to be_truthy
      end

      it 'sets access_token' do
        user.sign_in
        expect(user.access_token).to be_present
      end
    end
  end

  describe '.show' do
    context 'with valid user id' do
      let(:response_body) { { user: user } }

      before { allow(client_service).to receive(:show).and_return(Success(response_body.as_json)) }

      it 'returns user object' do
        expect(User.show(id: user.id, token: token)).to be_a_kind_of(User)
      end
    end

    context 'with invalid user id' do
      before { allow(client_service).to receive(:show).and_return(failure_reponse) }

      it 'returns nil' do
        expect(User.show(id: user.id, token: token)).to be_nil
      end
    end
  end

  describe '#widgets' do
    context 'for success request' do
      let(:response_body) { { widgets: attributes_for_list(:widget, 2) } }

      before { allow(client_service).to receive(:user_widgets).and_return(Success(response_body)) }

      it 'returns user object' do
        expect(user.widgets(token).first).to be_a_kind_of(Widget)
      end
    end

    context 'for failure request' do
      before { allow(client_service).to receive(:user_widgets).and_return(failure_reponse) }

      it 'returns empty array' do
        expect(user.widgets(token)).to be_empty
      end
    end
  end
end
