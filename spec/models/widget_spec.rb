require 'rails_helper'

describe Widget do
  let(:widget) { build(:widget) }
  let(:api_client) { ShowoffAPI::Clients::Widgets }
  let(:client_service) { instance_double(api_client) }
  let(:token) { 'test_access_token' }
  before { allow(api_client).to receive(:new).and_return(client_service) }

  describe 'Validations' do
    it 'is valid with all valid attributes' do
      expect(widget).to be_valid
    end

    it 'is not valid without name' do
      widget.name = nil
      expect(widget).to_not be_valid
    end

    it 'is not valid without description' do
      widget.description = nil
      expect(widget).to_not be_valid
    end

    it 'is not valid without kind' do
      widget.kind = nil
      expect(widget).to_not be_valid
    end
  end

  describe '.get_visible' do
    let(:response_body) { { widgets: attributes_for_list(:widget, 2) } }

    before { allow(client_service).to receive(:visible).and_return(Success(response_body)) }

    it 'delegates method to client' do
      expect(client_service).to receive(:visible)
      Widget.get_visible
    end

    it 'returns array of widgets' do
      response = Widget.get_visible
      expect(response.first).to be_kind_of(Widget)
    end

    context 'With invalid search term' do
      let(:response_body) { { widgets: [] } }

      it 'returns empty array' do
        response = Widget.get_visible
        expect(response).to be_empty
      end
    end
  end

  describe '#save' do
    context 'with invalid widget params' do
      before { widget.name = '' }

      it 'returns false' do
        expect(widget.save(token)).to be_falsy
      end

      it 'sets error message' do
        widget.save(token)
        expect(widget.errors[:name]).to be_present
      end
    end

    context 'with valid user params' do
      context 'with api server error' do
        before { allow(client_service).to receive(:create).and_return(failure_reponse) }

        it 'returns false' do
          expect(widget.save(token)).to be_falsy
        end

        it 'sets error message' do
          widget.save(token)
          expect(widget.errors[:base]).to be_present
        end
      end

      context 'with api success request' do
        let(:response_body) { { widget: attributes_for(:widget) } }

        before { allow(client_service).to receive(:create).and_return(Success(response_body)) }

        it 'returns true' do
          expect(widget.save(token)).to be_truthy
        end

        it 'sets widget id' do
          widget.id = nil
          widget.save(token)
          expect(widget.id).to be_present
        end
      end
    end
  end
end
