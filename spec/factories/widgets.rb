FactoryBot.define do
  factory :widget do
    id { 1 }
    name { 'test widget' }
    description { 'test desc' }
    kind { 'visible' }
    user
  end
end
