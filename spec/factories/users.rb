FactoryBot.define do
  factory :user do
    id { 1 }
    first_name { 'John' }
    last_name { 'Doe' }
    sequence(:email) { |n| "user#{n}@example.com" }
    password { 'secret' }
    password_confirmation { password }
  end
end
