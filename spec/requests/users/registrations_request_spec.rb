require 'rails_helper'

describe 'Users::Registrations' do

  let(:client_service) { instance_double(ShowoffAPI::Clients::Users) }
  let(:response_body) do
    {
      user: attributes_for(:user),
      token: {
        access_token: 'test_access_token',
        refresh_token: 'test_refresh_token'
      }
    }
  end

  before { allow(ShowoffAPI::Clients::Users).to receive(:new).and_return(client_service) }

  describe 'POST /users/sign_up' do
    context 'With valid user data' do
      let(:user_params) { attributes_for(:user) }

      before { allow(client_service).to receive(:create).and_return(Success(response_body)) }

      it 'redirects to home page' do
        post '/users/sign_up', params: { user:  user_params }, headers: { 'ACCEPT' => 'application/javascript' }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'With invalid data' do
      let(:user_params) { attributes_for(:user, email: '') }

      before { allow(client_service).to receive(:create).and_return(failure_reponse) }

      it 'renders signup form again' do
        post '/users/sign_up', params: { user:  user_params }, headers: { 'ACCEPT' => 'application/javascript' }
        expect(response).to render_template('users/registrations/_signup_form')
      end
    end
  end
end
