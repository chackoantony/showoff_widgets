require 'rails_helper'

describe 'Users::Sessions' do
  describe 'DELETE /users/sign_out' do
    it 'redirects to home page' do
      delete '/users/sign_out'
      expect(response).to redirect_to(root_path)
    end
  end

  describe 'POST /users/sign_in' do
    let(:client_service) { instance_double(ShowoffAPI::Clients::Users) }
    let(:response_body) do
      {
        user: attributes_for(:user),
        token: {
          access_token: 'test_access_token',
          refresh_token: 'test_refresh_token'
        }
      }
    end

    before { allow(ShowoffAPI::Clients::Users).to receive(:new).and_return(client_service) }
    context 'With valid credentials' do
      let(:user_params) { attributes_for(:user).slice(:email, :password) }

      before { allow(client_service).to receive(:sign_in).and_return(Success(response_body)) }

      it 'redirects to home page' do
        post '/users/sign_in', params: { user:  user_params }, headers: { 'ACCEPT' => 'application/javascript' }
        expect(response).to redirect_to(root_path)
      end
    end

    context 'With invalid credentials' do
      let(:user_params) { attributes_for(:user, email: '').slice(:email, :password) }

      before { allow(client_service).to receive(:sign_in).and_return(failure_reponse) }

      it 'renders sign in form again' do
        post '/users/sign_in', params: { user:  user_params }, headers: { 'ACCEPT' => 'application/javascript' }
        expect(response).to render_template('users/sessions/_signin_form')
      end
    end
  end
end
