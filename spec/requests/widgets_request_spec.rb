require 'rails_helper'

describe 'Widgets' do
  describe 'get /' do
    let(:widgets) { build_list(:widget, 2) }

    before do
      allow(Widget).to receive(:get_visible).and_return(widgets)
    end

    it 'sets widgets' do
      get '/'
      expect(assigns(:widgets).length).to eq widgets.length
    end
  end
end
