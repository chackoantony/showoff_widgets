# README

### Ruby version
2.6.3

### How to run the test suite
bundle exec rspec

### Deployment
App deployed to Heroku at https://hidden-castle-79600.herokuapp.com/

