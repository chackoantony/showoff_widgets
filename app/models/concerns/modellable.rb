module Modellable
  def build_widgets(response)
    response.value![:widgets].map do |widget_attrs|
      widget_attrs[:user] = User.new(widget_attrs[:user])
      Widget.new(widget_attrs)
    end
  end
end
