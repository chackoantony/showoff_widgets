class User
  include ActiveModel::Model
  include Modellable

  attr_accessor :id, :first_name, :last_name, :email, :password, :access_token, :refresh_token,
                :images, :name, :date_of_birth, :active

  validates :first_name, presence: true
  validates :email, presence: true, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :password, confirmation: true, presence: true
  validates :password_confirmation, presence: true

  class << self
    def show(id:, token:)
      response = ShowoffAPI::Clients::Users.new.show(id: id, token: token)
      return unless response.success?

      new(response.value![:user])
    end

    def current_user(token)
      response = ShowoffAPI::Clients::Users.new.me(token)
      return unless response.success?

      new(response.value![:user])
    end
  end

  def save
    return false unless valid?

    response = ShowoffAPI::Clients::Users.new.create(user: to_valid_params)
    if response.success?
      store_tokens(response) && true
    else
      (errors[:base] << response.failure.body[:message]) && false
    end
  end

  def sign_in
    response = ShowoffAPI::Clients::Users.new.sign_in({ username: email, password: password })
    if response.success?
      store_tokens(response) && true
    else
      (errors[:base] << response.failure.body[:message]) && false
    end
  end

  def widgets(token)
    response = ShowoffAPI::Clients::Users.new.user_widgets(id: id, token: token)
    if response.success?
      build_widgets(response)
    else
      []
    end
  end

  private

  def to_valid_params
    as_json(only: %w[first_name last_name email password])
  end

  def store_tokens(response)
    self.access_token = response.value!.dig(:token, :access_token)
    self.refresh_token = response.value!.dig(:token, :refresh_token)
  end
end
