class Widget
  include ActiveModel::Model

  attr_accessor :id, :name, :description, :kind, :user, :owner

  validates :name, presence: true
  validates :description, presence: true
  validates :kind, presence: true

  def persisted?
    id.present?
  end

  class << self
    include Modellable

    def get_visible(term = nil)
      response = ShowoffAPI::Clients::Widgets.new.visible(term)
      if response.success?
        build_widgets(response)
      else
        []
      end
    end

    def show(id:, token:)
      response = ShowoffAPI::Clients::Widgets.new.show(id: id, token: token)
      return unless response.success?

      new(response.value![:widget])
    end
  end

  def save(access_token)
    return unless valid?

    response = ShowoffAPI::Clients::Widgets.new.create(token: access_token, params: { widget: to_valid_params })
    if response.success?
      self.id = response.value![:widget][:id]
    else
      (errors[:base] << response.failure.body[:message]) && false
    end
  end

  def update(access_token)
    return unless valid?

    response = ShowoffAPI::Clients::Widgets.new.update(token: access_token, id: id, params: { widget: to_valid_params })
    if response.success?
      true
    else
      (errors[:base] << response.failure.body[:message]) && false
    end
  end

  def destroy(access_token)
    response = ShowoffAPI::Clients::Widgets.new.destroy(token: access_token, id: id)
    if response.success?
      true
    else
      (errors[:base] << response.failure.body[:message]) && false
    end
  end

  def owned_by?(user)
    return false unless user

    self.user&.id == user.id
  end

  private

  def to_valid_params
    as_json(only: %w[name description kind])
  end
end
