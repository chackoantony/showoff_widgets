module ShowoffAPI
  CLIENT_ID = Rails.configuration.showoff_api[:client_id]
  CLIENT_SECRET = Rails.configuration.showoff_api[:client_secret]
end
