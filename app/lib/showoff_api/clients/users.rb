module ShowoffAPI
  module Clients
    class Users < ApiStruct::Client
      include ShowoffAPI::Helpers
      showoff_api 'api/v1/users'

      def create(params)
        post(json: api_credentials.merge(params)).fmap { |i| i[:data] }
      end

      def sign_in(params)
        params.merge!(grant_type: 'password').merge!(api_credentials)
        post(path: 'oauth/token', json: params).fmap { |i| i[:data] }
      end

      def show(id:, token:)
        get(id, headers: auth_header(token)).fmap { |i| i[:data] }
      end

      def me(token)
        get('me', headers: auth_header(token)).fmap { |i| i[:data] }
      end

      def user_widgets(id:, token:)
        get("#{id}/widgets", params: api_credentials, headers: auth_header(token)).fmap { |i| i[:data] }
      end
    end
  end
end
