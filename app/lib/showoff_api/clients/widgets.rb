module ShowoffAPI
  module Clients
    class Widgets < ApiStruct::Client
      include ShowoffAPI::Helpers
      showoff_api 'api/v1/widgets'

      def visible(search_term = nil)
        params = { term: search_term }.merge(api_credentials).compact
        get('visible', params: params).fmap { |i| i[:data] }
      end

      def create(token:, params:)
        post(json: params, headers: auth_header(token)).fmap { |i| i[:data] }
      end

      def show(id:, token:)
        get(id, headers: auth_header(token)).fmap { |i| i[:data] }
      end

      def update(token:, id:, params:)
        put(id, json: params, headers: auth_header(token)).fmap { |i| i[:data] }
      end

      def destroy(token:, id:)
        delete(id, headers: auth_header(token)).fmap { |i| i[:data] }
      end
    end
  end
end
