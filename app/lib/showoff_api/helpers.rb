module ShowoffAPI
  module Helpers
    def api_credentials
      { client_id: ShowoffAPI::CLIENT_ID, client_secret: ShowoffAPI::CLIENT_SECRET }
    end

    def auth_header(token)
      { 'Authorization': "Bearer #{token}" }
    end
  end
end
