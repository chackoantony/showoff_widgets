class Users::RegistrationsController < ApplicationController
  include SessionHelper
  skip_before_action :authenticate_user!

  def create
    @user = User.new(user_params)
    if @user.save
      set_session(@user)
      redirect_to root_path, notice: 'Succesfully registered as new user.'
    end
  end

  private

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end
end
