class Users::SessionsController < ApplicationController
  include SessionHelper

  skip_before_action :authenticate_user!, only: [:create]

  def create
    @user = User.new(login_params)
    if @user.sign_in
      set_session(@user)
      redirect_to root_path, notice: 'Succesfully signed in.'
    end
  end

  def destroy
    reset_session
    redirect_to root_path, notice: 'You have been logged out.'
  end

  private

  def login_params
    params.require(:user).permit(:email, :password)
  end
end
