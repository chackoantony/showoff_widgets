module SessionHelper
  def set_session(user)
    session[:access_token] = user.access_token
    session[:refresh_token] = user.refresh_token
  end
end
