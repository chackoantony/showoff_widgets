class ApplicationController < ActionController::Base
  layout proc{ |c| c.request.xhr? ? false : 'application' }

  before_action :authenticate_user!

  protected

  def user_authenticated?
    session[:access_token].present?
  end

  def authenticate_user!
    return true if user_authenticated?

    flash[:notice] = 'Please signin to continue.'
    redirect_to root_path
  end

  def current_user
    return unless user_authenticated?
    @current_user ||= User.current_user(access_token)
  end

  helper_method :user_authenticated?
  helper_method :current_user

  private

  def access_token
    @access_token ||= session[:access_token]
  end

  def refresh_token
    @refresh_token ||= session[:refresh_token]
  end
end
