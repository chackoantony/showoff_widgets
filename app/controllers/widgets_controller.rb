class WidgetsController < ApplicationController
  skip_before_action :authenticate_user!, only: [:index]

  def new
    @widget = Widget.new
  end

  def index
    @widgets = Widget.get_visible(params[:term])
    @widgets = Kaminari.paginate_array(@widgets).page(params[:page])
  end

  def edit
    @widget = Widget.show(id: params[:id], token: access_token)
    render 'new'
  end

  def create
    @widget = Widget.new(widget_params)
    @widget.user = current_user
    @widget.save(access_token)
  end

  def update
    @widget = Widget.new(widget_params.merge(id: params[:id]))
    @widget.user = current_user
    @widget.update(access_token)
  end

  def destroy
    @widget = Widget.show(id: params[:id], token: access_token)
    @widget.destroy(access_token)
  end

  private

  def widget_params
    params.require(:widget).permit(:name, :description, :kind)
  end
end
