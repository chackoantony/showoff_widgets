class UsersController < ApplicationController
  def show
    @user = User.show(id: params[:id], token: access_token)
    @widgets = Kaminari.paginate_array(@user.widgets(access_token)).page(params[:page])
  end
end
