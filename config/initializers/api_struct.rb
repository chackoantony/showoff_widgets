ApiStruct::Settings.configure do |config|
  config.endpoints = {
    showoff_api: {
      root: Rails.configuration.showoff_api[:url],
      headers: {
        'content-type': 'application/json'
      }
    }
  }
end
