Rails.application.routes.draw do
  get 'users/show'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'widgets#index'

  namespace :users do
    post :sign_up, to: 'registrations#create'
    post :sign_in, to: 'sessions#create'
    delete :sign_out, to: 'sessions#destroy'
  end

  resources :users
  resources :widgets
end
